<?php

namespace TemplateManager;

/**
 * Class TemplateManager
 * @package TemplateManager
 */
class TemplateManager
{
    /**
     * @var string
     */
    private $template;

    /**
     * @var array
     */
    private $config;

    /**
     * TemplateManager constructor.
     *
     * @param string     $template
     * @param array|null $config
     */
    public function __construct(string $template, array $config = null)
    {
        $inputStart = ($config !== null && isset($config['inputStart'])) ? $config['inputStart'] : '{{';
        $inputEnd   = ($config !== null && isset($config['inputEnd'])) ? $config['inputEnd'] : '}}';

        $this->config = [
            'inputStart' => $inputStart,
            'inputEnd'   => $inputEnd,
        ];

        $this->template = $this->prepareTemplate($template);
    }

    /**
     * @return array
     */
    public function extractInputs()
    {
        $separatedTemplate = $this->separate($this->template);

        $result = [];

        $startNeedle = $this->config['inputStart'];

        foreach ($separatedTemplate as $part) {
            if (strpos($part, $startNeedle) === 0) {
                $result[] = substr($part, 2, -2);
            }
        }

        return array_unique($result);
    }

    /**
     * @param array $input
     *
     * @return string
     */
    public function getText(array $input): string
    {
        $result = $this->template;

        foreach ($input as $key => $value) {
            $result = str_replace('{{' . $key . '}}', $value, $result);
        }

        return $result;
    }

    /**
     * @param string $template
     *
     * @return string
     */
    private function prepareTemplate(string $template): string
    {
        $separatedTemplate = $this->separate($template);

        $preparedSeparatedTemplate = [];

        $startNeedle = $this->config['inputStart'];

        foreach ($separatedTemplate as $part) {
            if (strpos($part, $startNeedle) === 0) {
                $preparedSeparatedTemplate[] = $this->trimInput($part);
                continue;
            }
            $preparedSeparatedTemplate[] = $part;
        }

        return implode('', $preparedSeparatedTemplate);
    }

    /**
     * @param string $input
     *
     * @return string
     */
    private function trimInput(string $input): string
    {
        return '{{' . trim(substr($input, 2, -2)) . '}}';
    }

    /**
     * @param string $haystack
     *
     * @return array
     * @throws \Exception
     */
    private function separate(string $haystack): array
    {
        $endReached = false;

        $startNeedleFound = false;
        $startNeedle      = $this->config['inputStart'];

        $endNeedleFound  = false;
        $endNeedle       = $this->config['inputEnd'];
        $endNeedleLength = strlen($endNeedle);

        $result     = [];
        $leftString = '';

        while (! $endReached) {
            if (! $startNeedleFound && ! $endNeedleFound) {
                $strpos = strpos($haystack, $startNeedle);

                if ($strpos === false) {
                    $result[]   = $haystack;
                    $endReached = true;
                    continue;
                }

                $result[]   = substr($haystack, 0, $strpos);
                $leftString = substr($haystack, $strpos);

                $startNeedleFound = true;

                continue;
            }

            if ($startNeedleFound && ! $endNeedleFound) {
                $strpos = strpos($leftString, $endNeedle);

                if ($strpos === false) {
                    throw new \Exception('Incorrect syntax');
                }

                $result[]   = substr($leftString, 0, $strpos + $endNeedleLength);
                $leftString = substr($leftString, $strpos + $endNeedleLength);

                $startNeedleFound = false;
                $endNeedleFound   = true;

                continue;
            }

            if (! $startNeedleFound && $endNeedleFound) {
                $strpos = strpos($leftString, $startNeedle);

                if ($strpos === false) {
                    $result[]   = $leftString;
                    $endReached = true;
                    continue;
                }

                $result[]   = substr($leftString, 0, $strpos);
                $leftString = substr($leftString, $strpos);

                $startNeedleFound = true;
                $endNeedleFound   = false;
            }
        }

        return $result;
    }
}
