<?php

namespace TemplateManager\Test\TestCase;

use TemplateManager\TemplateManager;
use PHPUnit\Framework\TestCase;

/**
 * Class TemplateManagerTest
 * @package TemplateManager\Test\TestCase
 */
class TemplateManagerTest extends TestCase
{
    public function testGetText()
    {
        $manager = new TemplateManager('Hi, {{name  }}! Today is {{ date}}.');

        $this->assertEquals("Hi, Max! Today is 01.01.2000.", $manager->getText([
            'name' => 'Max',
            'date' => '01.01.2000',
        ]));
    }

    public function testExtractInputs()
    {
        $manager = new TemplateManager('Hi, {{name  }}! Today is {{ date}}.');

        $this->assertEquals(['name', 'date'], $manager->extractInputs());
    }

    public function testExtractInputs_duplication()
    {
        $manager = new TemplateManager('Hi, {{name  }}!{{name}} is cool.');

        $this->assertEquals(['name'], $manager->extractInputs());
    }

    public function testExtractInputs_inputPos0()
    {
        $manager = new TemplateManager('{{var}} - {{var}}');

        $this->assertEquals(['var'], $manager->extractInputs());
    }
}
